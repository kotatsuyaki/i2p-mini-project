# Mini shooting game for I2P final project (id: 107021129)

- [Dependencies](#dependencies)
- [Building](#building)
- [Credits](#credits)
- [On Scoring for the Course](#on-scoring-for-the-course)

# Dependencies

App dependencies

- [liballegro](https://liballeg.org/), available from their site and most package managers.

Build dependencies

- unix-like environment
- make
- pkg-config
- GCC-compatible compiler (gcc, clang etc.) with c11 support

# Folder Structure

```
$ tree -L 1 -F
.
├── Doxyfile
├── Makefile
├── README.md
├── assets/   # game assets (textures, audio etc.)
├── bin/      # output binary file
├── build/    # building dir
└── src/      # *.c *.h source files
```

# Building

Use the included `Makeflie` *(only tested on Arch Linux and MSYS2)*[^1]

[^1]: On Msys2 it's required to use `pacman` to install `mingw-w64-x86_64-allegro` `mingw-w64-x86_64-gcc` `make` `pkg-config` etc.

```BASH
$ make clean
$ make
```

Note that you should have all the dependencies installed, and have the environment variable `$LD_LIBRARY_PATH` set up correctly. This depends on which shell you're using:

```BASH
# Append the library path containing liballegro.*\.so
# bash, zsh etc.
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib
# fish
set -Ux LD_LIBRARY_PATH /usr/lib $LD_LIBRARY_PATH
```

The compiled binary (as well as the assets) will be under the `bin/` directory. To run it, either use `make run`, or manually `cd bin/` and run the `game` binary.

# On Scoring for the Course

I'll just put a checklist here in case the TA wants it.

1. Basics
    - [x] Scoring in game.
    - [x] Plane (cannot exceed the displayboundaries), Enemies、Bullets can move correctly.
    - [x] Bullets can deal damage.
    - [x] Health Bar(HP)／Remaining lives (lives) **(on the bottom-right corner)**
    - [x] Use mouse and keyboard
    - [x] The 4th scene (we already have Menu, Start, Settings) **(a very primitive settings page)**
2. Advanced
    - [ ] Opening Animation+ Character Animation
    - [x] Permanent Scoring (High Score／Save Game) **(saves highest score in `store.dat`)**
    - [x] 2.5D Scene **(...if the moving paralax background counts)**
    - [ ] 2P Mode(Cooperate shoot enemies)
    - [ ] Character Select
    - [x] Sound Effects + Background Music(different music in different scene) **(shooting sound & enemy killed sound effect)**
    - [ ] Boss
    - [x] Ultimate (Ult) / SpecialAttack **(critical attack deals 3pt of damage)**

# Credits

The code is modified the template provided by the course, available at [their github repo](https://github.com/j3soon/Allegro5Template).
