#ifndef SCENE_GAMEOVER_H
#define SCENE_GAMEOVER_H

#include "game.h"
#include "scene_start.h"

Scene scene_gameover_create(PlayerData pd);

#endif
