#include "scene_settings.h"
#include "scene_menu.h"
#include "shared.h"
#include "utility.h"
#include "button.h"

#include <allegro5/allegro_primitives.h>

static ALLEGRO_BITMAP* img_settings_background;

static Button mute_btn;
static Button unmute_btn;
static Button main_menu_btn;

static void draw_mute_btn() {
    if (muted) {
        draw_button(&unmute_btn);
    } else {
        draw_button(&mute_btn);
    }
}

static void draw() {
    al_draw_bitmap(img_settings_background, 0, 0, 0);
    al_draw_text(font_pirulen_32, al_map_rgb(255, 255, 255), SCREEN_W / 2, 30,
                 ALLEGRO_ALIGN_CENTER, "Settings");

    draw_grey_bg_box();
    draw_mute_btn();
    draw_button(&main_menu_btn);
}

static void init() {
    img_settings_background =
        load_bitmap_resized("assets/settings-bg.jpg", SCREEN_W, SCREEN_H);

    mute_btn = new_button(SCREEN_W / 2 - 100, 100, 200, 38, img_mute, img_mute_p);
    unmute_btn = new_button(SCREEN_W / 2 - 100, 100, 200, 38, img_unmute, img_unmute_p);
    main_menu_btn = new_button(SCREEN_W - 210, SCREEN_H - 48, 200, 38, img_main_menu, img_main_menu_pressed);
}

static void on_key_down(int keycode) {
    if (keycode == ALLEGRO_KEY_Q) {
        game_change_scene(scene_menu_create());
    }
}

static void on_mouse_down(int btn, int x, int y, int dz) {
    if (clicked(&mute_btn, btn))
        muted = !muted;
    if (clicked(&main_menu_btn, btn))
        game_change_scene(scene_menu_create());
}


// The only function that is shared across files.
Scene scene_settings_create(void) {
    Scene scene;
    memset(&scene, 0, sizeof(Scene));
    scene.name        = "Start";
    scene.draw        = &draw;
    scene.initialize  = &init;
    scene.on_key_down = &on_key_down;
    scene.on_mouse_down = &on_mouse_down;
    game_log("Settings scene created");
    return scene;
}
