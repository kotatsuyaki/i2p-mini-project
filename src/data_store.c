#include "data_store.h"

#include <stdio.h>

const char* store_fname = "store.dat";
DataStore ds;

void load_data_store() {
    FILE* fp = fopen(store_fname, "r");
    if (fp)
        fscanf(fp, "%d", &ds.hi_score);
    else
        ds.hi_score = 0;
}

int get_hi_score() {
    return ds.hi_score;
}

void set_hi_score(int s) {
    ds.hi_score = s;
    FILE* fp = fopen(store_fname, "w");
    fprintf(fp, "%d", ds.hi_score);
}
