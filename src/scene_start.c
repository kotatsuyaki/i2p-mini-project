#include "scene_start.h"
#include "game.h"
#include "scene_gameover.h"
#include "shared.h"
#include "utility.h"

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <math.h>
#include <stdio.h>
#include <string.h>

static int bg_w, bg_h;

typedef struct {
    bool wobbles;
    int cd;
    int offset;
} WobbleStatus;

typedef struct {
    float xi, yi, xf, yf;
} AABB;

typedef enum { Enemy, Bullet, Plane } ObjType;

typedef struct {
    // The center coordinate of the image.
    float x, y;
    // The width and height of the object.
    float w, h;
    // The velocity in x, y axes.
    float vx, vy;
    // Should we draw this object on the screen.
    bool hidden;
    // The pointer to the object’s image.
    ALLEGRO_BITMAP* img;
    WobbleStatus wb;
    ObjType type;
} MovableObject;

typedef struct _PinText {
    int x, y;
    ALLEGRO_COLOR color;
    int lifetime;
    char text[30];
    struct _PinText* next;
    struct _PinText* prev;
} PinText;

static PinText* pt_head = NULL;

PinText* new_pintext(int x, int y, const char* text, ALLEGRO_COLOR color) {
    PinText* pt = malloc(sizeof(PinText));
    pt->x       = x;
    pt->y       = y;
    pt->color   = color;
    pt->lifetime = 100;
    strncpy(pt->text, text, 30);
    return pt;
}

void push_pintext(PinText* pt) {
    pt->next = pt_head;
    if (pt_head)
        pt_head->prev = pt;
    pt_head = pt;
}

PinText* del_pintext(PinText* pt) {
    if (pt == pt_head) {
        pt_head = pt_head->next;
        return pt_head;
    }

    if (pt->prev)
        pt->prev->next = pt->next;

    PinText* next = pt->next;
    if (pt->next)
        pt->next->prev = pt->prev;

    free(pt);
    return next;
}

typedef struct {
    unsigned int cd;
    float v;
    HP hp;
} EnemyData;

static void init(void);
static void update(void);
static void draw_movable_object(MovableObject* obj, bool flipped);
static void draw(void);
static void destroy(void);

#define MAX_ENEMY 10
#define MAX_BULLET 4
#define MAX_E_BULLET 400

static MovableObject plane;
static MovableObject enemies[MAX_ENEMY];
static MovableObject bullets[MAX_BULLET];
static MovableObject e_bullets[MAX_E_BULLET];

static EnemyData enemy_data[MAX_ENEMY];
static PlayerData pd;

static const float MAX_COOLDOWN = 0.2f;
static double last_shoot_timestamp;
static ALLEGRO_SAMPLE_ID bgm_id;
static bool draw_gizmos;

static void respawn_enemy(MovableObject* e, EnemyData* ed);

static void init_plane() {
    plane.img        = img_plane;
    plane.x          = 400;
    plane.y          = 500;
    plane.vx         = 0;
    plane.vy         = 0;
    plane.w          = al_get_bitmap_width(plane.img);
    plane.h          = al_get_bitmap_height(plane.img);
    plane.wb.wobbles = false;
    plane.wb.cd      = 0;
    plane.type       = Plane;
}

static void init_enemies() {
    for (int i = 0; i < MAX_ENEMY; i++) {
        enemies[i].img        = img_enemy;
        enemies[i].w          = al_get_bitmap_width(img_enemy);
        enemies[i].h          = al_get_bitmap_height(img_enemy);
        enemies[i].wb.wobbles = false;
        enemies[i].wb.cd      = 0;
        enemies[i].type       = Enemy;
        respawn_enemy(&enemies[i], &enemy_data[i]);
    }
}

static void init_bullets() {
    for (int i = 0; i < MAX_BULLET; i++) {
        bullets[i].img        = img_bullet;
        bullets[i].w          = al_get_bitmap_width(img_bullet);
        bullets[i].h          = al_get_bitmap_height(img_bullet);
        bullets[i].wb.wobbles = false;
        bullets[i].wb.cd      = 0;
        bullets[i].vx         = 0;
        bullets[i].vy         = -3;
        bullets[i].hidden     = true;
        bullets[i].type       = Bullet;
    }

    for (int i = 0; i < MAX_E_BULLET; i++) {
        e_bullets[i].img        = img_bullet;
        e_bullets[i].w          = al_get_bitmap_width(img_bullet);
        e_bullets[i].h          = al_get_bitmap_height(img_bullet);
        e_bullets[i].wb.wobbles = false;
        e_bullets[i].wb.cd      = 0;
        e_bullets[i].vx         = 0;
        e_bullets[i].vy         = 3;
        e_bullets[i].hidden     = true;
        e_bullets[i].type       = Bullet;
    }
}

static void init_player() {
    pd.hp.value     = 10;
    pd.remain_lives = 3;
    pd.score        = 0;

    pd.kills        = 0;
    pd.bullet_count = 0;
    pd.hit_count    = 0;
    pd.start_time   = al_get_time();
}

static void init(void) {
    bg_w = al_get_bitmap_width(img_sky);
    bg_h = al_get_bitmap_height(img_sky);

    init_plane();
    init_enemies();
    init_bullets();
    init_player();

    game_log("Start scene initialized");
    bgm_id = play_bgm(game_bgm, get_volume());
}

static void clamp(float* num, const float lo, const float hi) {
    if (*num < lo)
        *num = lo;
    if (*num > hi)
        *num = hi;
}

static void get_drag(float* dx, float* dy, float* vx, float* vy, float coeff) {
    if (*vx) {
        *dx = *vx / fabs(*vx) * coeff;
    } else {
        *dx = 0;
    }
    if (*vy) {
        *dy = *vy / fabs(*vy) * coeff;
    } else {
        *dy = 0;
    }
}

static void normalize_clamp(float* vx, float* vy, const float max_len) {
    float len = sqrtf((*vx) * (*vx) + (*vy) * (*vy));
    if (len <= max_len)
        return;

    *vx = *vx * (max_len / len);
    *vy = *vy * (max_len / len);
}

static void update_plane() {
    float drag_x, drag_y;
    get_drag(&drag_x, &drag_y, &(plane.vx), &(plane.vy), 0.05);

    float new_vx = plane.vx - drag_x;
    float new_vy = plane.vy - drag_y;

    // If signs different
    if (!((new_vx >= 0) ^ (plane.vx < 0))) {
        new_vx = 0;
    }
    if (!((new_vy >= 0) ^ (plane.vy < 0))) {
        new_vy = 0;
    }

    if (key_state[ALLEGRO_KEY_UP] || key_state[ALLEGRO_KEY_W])
        new_vy -= 1;
    if (key_state[ALLEGRO_KEY_DOWN] || key_state[ALLEGRO_KEY_S])
        new_vy += 1;
    if (key_state[ALLEGRO_KEY_LEFT] || key_state[ALLEGRO_KEY_A])
        new_vx -= 1;
    if (key_state[ALLEGRO_KEY_RIGHT] || key_state[ALLEGRO_KEY_D])
        new_vx += 1;

    clamp(&new_vx, -4, 4);
    clamp(&new_vy, -4, 4);

    normalize_clamp(&new_vx, &new_vy, 2.828f);

    plane.vx = new_vx;
    plane.vy = new_vy;

    plane.y += plane.vy * 2;
    plane.x += plane.vx * 2;

    // Limit bounding box on screen
    if (plane.x - 25 < 0)
        plane.x = 25;
    else if (plane.x + 25 > SCREEN_W)
        plane.x = SCREEN_W - 25;
    if (plane.y - 25 < 0)
        plane.y = 25;
    else if (plane.y + 25 > SCREEN_H)
        plane.y = SCREEN_H - 25;
}

static AABB get_aabb_with_padding(MovableObject* obj, float padding) {
    AABB box = {
        obj->x - obj->w / 2 - padding,
        obj->y - obj->h / 2 - padding,
        obj->x + obj->w / 2 + padding,
        obj->y + obj->h / 2 + padding
    };
    return box;
}

// Get bounding box of the object
static AABB get_aabb(MovableObject* obj) {
    // Make the player's box smaller to gratify the player's varnity
    if (obj->type == Plane) {
        return get_aabb_with_padding(obj, -15);
    }

    AABB box = {
        obj->x - obj->w / 2,
        obj->y - obj->h / 2,
        obj->x + obj->w / 2,
        obj->y + obj->h / 2
    };
    return box;
}

static bool collides(MovableObject* o1, MovableObject* o2) {
    AABB box_a = get_aabb(o1);
    AABB box_b = get_aabb(o2);

    if (box_a.xi < box_b.xf && box_a.xf > box_b.xi && box_a.yi < box_b.yf &&
        box_a.yf > box_b.yi) {
        return true;
    }

    return false;
}

static void set_wobble(MovableObject* e, int cd, int offset) {
    e->wb.cd      = cd;
    e->wb.offset  = offset;
    e->wb.wobbles = true;
}

static bool should_die() { return pd.hp.value <= 0; }

static void go_gameover() {
    pd.end_time = al_get_time();
    PinText* cur = pt_head;
    while (cur)
        cur = del_pintext(cur);
    game_change_scene(scene_gameover_create(pd));
}

static void dec_lives() {
    if (--pd.remain_lives == 0) {
        go_gameover();
    } else {
        pd.hp.value = 10;
        pd.hp.cd    = 20;
    }
}

static void take_damage_plane(MovableObject* b) {
    if (pd.hp.cd > 0)
        pd.hp.cd--;

    if (pd.hp.cd <= 0) {
        pd.hp.value--;

        pd.hp.cd = 10;

        set_wobble(&plane, 30, 5);

        b->hidden = true;
        printf("hp %d\n", pd.hp.value);

        PinText* pt = new_pintext(plane.x, plane.y - plane.h / 2, "-1", al_map_rgb(0xff, 0x00, 0x00));
        push_pintext(pt);
    }

    if (should_die()) {
        dec_lives();
    }
}

static void take_damage(MovableObject* e, EnemyData* ed, MovableObject* b) {
    // dec enemy damage CD
    if (ed->hp.cd > 0)
        ed->hp.cd--;

    if (ed->hp.cd <= 0) {
        bool is_critical = one_out_of(5);

        pd.score += 1;

        // dec enemy HP
        PinText* pt;
        if (is_critical) {
            ed->hp.value -= 3;
            pt = new_pintext(e->x, e->y - e->h / 2, "-3 CRITICAL", al_map_rgb(0xff, 0x00, 0x00));
        } else {
            ed->hp.value--;
            pt = new_pintext(e->x, e->y - e->h / 2, "-1", al_map_rgb(0xff, 0x00, 0x00));
        }
        push_pintext(pt);

        // set enemy damage CD
        ed->hp.cd = 10;

        // wobble the enemy
        set_wobble(e, 30, 5);

        // recycle bullet
        b->hidden = true;
        printf("hp %d\n", ed->hp.value);
        pd.hit_count++;

    }

    // Test if enemy should die
    if (ed->hp.value <= 0) {
        // recycle enemy
        e->hidden = true;
        ed->cd    = 100;
        pd.score += 10;
        pd.kills++;
        play_audio(explosion_fx, get_volume());
    }
}

static bool should_take_damage(MovableObject* e, MovableObject* b) {
    return !e->hidden && collides(b, e);
}

static void update_bullets() {
    for (int i = 0; i < MAX_BULLET; i++) {
        if (bullets[i].hidden)
            continue;

        bullets[i].x += bullets[i].vx;
        bullets[i].y += bullets[i].vy;
        if (bullets[i].y < 0)
            bullets[i].hidden = true;

        for (int j = 0; j < MAX_ENEMY; j++) {
            if (should_take_damage(&enemies[j], &bullets[i])) {
                take_damage(enemies + j, enemy_data + j, bullets + i);
            }
        }
    }
}

static void update_enemy_bullets() {
    for (int i = 0; i < MAX_E_BULLET; i++) {
        if (e_bullets[i].hidden)
            continue;

        e_bullets[i].x += e_bullets[i].vx;
        e_bullets[i].y += e_bullets[i].vy;
        if (e_bullets[i].y > SCREEN_H)
            e_bullets[i].hidden = true;

        if (should_take_damage(&plane, e_bullets + i)) {
            take_damage_plane(e_bullets + i);
        }
    }
}

static void update_enemy(MovableObject* e, EnemyData* ed) {
    e->x += ed->v;

    if (e->x - 25 < 0) {
        e->x  = 25;
        ed->v = -(ed->v);
    } else if (e->x + 25 > SCREEN_W) {
        e->x  = SCREEN_W - 25;
        ed->v = -(ed->v);
    }
}

static void update_enemies() {
    for (int i = 0; i < MAX_ENEMY; i++) {
        update_enemy(&enemies[i], &enemy_data[i]);
    }
}

static void update_try_shoot() {
    double now = al_get_time();

    // shoot
    if (key_state[ALLEGRO_KEY_SPACE] &&
        now - last_shoot_timestamp >= MAX_COOLDOWN) {
        int i;
        play_audio(beam_fx, get_volume());

        for (i = 0; i < MAX_BULLET; i++) {
            if (bullets[i].hidden)
                break;
        }

        if (i < MAX_BULLET) {
            last_shoot_timestamp = now;
            bullets[i].hidden    = false;
            bullets[i].x         = plane.x;
            bullets[i].y         = plane.y - 25;
            pd.bullet_count++;
        }
    }
}

static void e_shoot(MovableObject* e, MovableObject* b) {
    b->hidden = false;
    b->x      = e->x;
    b->y      = e->y + 15;
}

static void update_enemy_try_shoot() {
    for (MovableObject* e = enemies; e < enemies + MAX_ENEMY; e++) {
        if (rand() % 20 == 0) {
            for (MovableObject* b = e_bullets; b < e_bullets + MAX_E_BULLET;
                 b++) {
                if (b->hidden) {
                    e_shoot(e, b);
                    break;
                }
            }
        }
    }
}

static void respawn_enemy(MovableObject* e, EnemyData* ed) {
    e->x         = (e->w) / 2 + (float)rand() / RAND_MAX * (SCREEN_W - e->w);
    e->y         = 80;
    e->hidden    = false;
    ed->v        = powf(rand() % 3 + 1, 2) / 2.0f * rand_sign();
    ed->hp.value = 5;
    ed->hp.cd    = 0;
}

static void update_spawn_enemies() {
    for (int i = 0; i < MAX_ENEMY; i++) {
        if (enemy_data[i].cd > 0) {
            enemy_data[i].cd--;
        }

        if (enemies[i].hidden == false) {
            continue;
        }

        if (enemy_data[i].cd == 0 && one_out_of(100)) {
            respawn_enemy(&enemies[i], &enemy_data[i]);
        }
    }
}

static void update(void) {
    update_plane();
    update_bullets();
    update_enemies();
    update_enemy_bullets();
    update_try_shoot();
    update_enemy_try_shoot();
    update_spawn_enemies();
}

static void draw_hp_bar() {
    int rect_xi = SCREEN_W - 300 - 10;
    int rect_yi = SCREEN_H - 30 - 10;
    int rect_xf = SCREEN_W - 10;
    int rect_yf = SCREEN_H - 10;
    float hp_xf = pd.hp.value / 10.0f * 300 + rect_xi;

    static char hp_text[50];
    sprintf(hp_text, "%.1f%%", pd.hp.value * 10.0f);

    al_draw_filled_rectangle(rect_xi, rect_yi, rect_xf, rect_yf,
                             al_map_rgba(80, 80, 80, 200));
    al_draw_filled_rectangle(rect_xi, rect_yi, hp_xf, rect_yf,
                             al_map_rgb(80, 80, 80));

    ALLEGRO_COLOR green  = al_map_rgb(0x00, 0xff, 0x00);
    ALLEGRO_COLOR red    = al_map_rgb(0xff, 0x00, 0x00);
    ALLEGRO_COLOR yellow = al_map_rgb(0xff, 0xff, 0x00);
    ALLEGRO_COLOR mixed =
        interpolate_color(red, yellow, green, pd.hp.value / 10.0f);

    al_draw_text(font_pirulen_24, mixed, SCREEN_W - 10 - 150,
                 SCREEN_H - 10 - 30, ALLEGRO_ALIGN_CENTER, hp_text);
}

static void draw_remain_lives() {
    int rect_xi = SCREEN_W - 380 - 10;
    int rect_yi = SCREEN_H - 30 - 10;
    int rect_xf = SCREEN_W - 310 - 10;
    int rect_yf = SCREEN_H - 10;

    static char remain_lives_text[30];
    sprintf(remain_lives_text, "%d", pd.remain_lives);

    al_draw_filled_rectangle(rect_xi, rect_yi, rect_xf, rect_yf,
                             al_map_rgba(80, 80, 80, 200));

    al_draw_text(font_pirulen_24, al_map_rgb(0xff, 0xff, 0xff),
                 (rect_xi + rect_xf) / 2, rect_yi, ALLEGRO_ALIGN_CENTER,
                 remain_lives_text);
}

static void draw_score() {
    int rect_xi = SCREEN_W - 200 - 10;
    int rect_yi = 10;
    int rect_xf = SCREEN_W - 10;
    int rect_yf = 50;

    static char score_text[30];
    sprintf(score_text, "Score: %03d", pd.score);

    al_draw_filled_rectangle(rect_xi, rect_yi, rect_xf, rect_yf,
                             al_map_rgba(80, 80, 80, 200));

    al_draw_text(font_pirulen_24, al_map_rgb(0xff, 0xff, 0xff),
                 (rect_xi + rect_xf) / 2, rect_yi, ALLEGRO_ALIGN_CENTER,
                 score_text);
}

static void draw_movable_object(MovableObject* obj, bool flipped) {
    if (obj->hidden)
        return;
    int obj_xi = round(obj->x - obj->w / 2);
    int obj_yi = round(obj->y - obj->h / 2);

    if (obj->wb.wobbles) {
        obj->wb.cd--;

        if (obj->wb.cd <= 0) {
            obj->wb.wobbles = false;
        }

        obj_xi += obj->wb.offset;
        obj_yi += obj->wb.offset * rand_sign();

        if (obj->wb.cd % 3 == 0)
            obj->wb.offset = -(obj->wb.offset);
    }

    al_draw_bitmap(obj->img, obj_xi, obj_yi,
                   flipped ? ALLEGRO_FLIP_VERTICAL : 0);
    if (draw_gizmos) {
        al_draw_rectangle(
            round(obj->x - obj->w / 2), round(obj->y - obj->h / 2),
            round(obj->x + obj->w / 2) + 1, round(obj->y + obj->h / 2) + 1,
            al_map_rgb(255, 0, 0), 0);
    }
}

static float get_bg_x() {
    float bg_x = (float)(plane.x - 25) / (float)(SCREEN_W - 50) *
                 (float)(bg_w - SCREEN_W);
    return -bg_x;
}

static int cur_bg_offset = 0;

static int get_bg_y_hi() {
    return SCREEN_H + cur_bg_offset - 2 * bg_h;
}

static int get_bg_y_lo() {
    return SCREEN_H + cur_bg_offset - bg_h;
}

static void draw_pt(PinText* pt) {
    al_draw_text(font_fmr, pt->color,
            pt->x, pt->y + pt->lifetime * 0.5, ALLEGRO_ALIGN_CENTER, pt->text);
}

static void draw_pintexts() {
    PinText* cur = pt_head;
    while (cur) {
        draw_pt(cur);

        cur->lifetime--;
        if (cur->lifetime < 0) {
            cur = del_pintext(cur);
        } else {
            cur = cur->next;
        }
    }
}

static void draw(void) {
    cur_bg_offset += 2;
    cur_bg_offset %= bg_h;
    al_draw_bitmap(img_sky, get_bg_x(), get_bg_y_hi(), 0);
    al_draw_bitmap(img_sky, get_bg_x(), get_bg_y_hi(), 0);
    al_draw_bitmap(img_sky, get_bg_x(), get_bg_y_lo(), 0);

    for (int i = 0; i < MAX_BULLET; i++) {
        draw_movable_object(&bullets[i], false);
    }

    for (int i = 0; i < MAX_E_BULLET; i++) {
        draw_movable_object(&e_bullets[i], true);
    }

    draw_movable_object(&plane, false);
    for (int i = 0; i < MAX_ENEMY; i++)
        draw_movable_object(&enemies[i], false);

    draw_hp_bar();
    draw_remain_lives();
    draw_score();
    draw_pintexts();
}

static void destroy(void) {
    stop_bgm(bgm_id);
    game_log("Start scene destroyed");
}

static void on_key_down(int keycode) {
    if (keycode == ALLEGRO_KEY_TAB)
        draw_gizmos = !draw_gizmos;
}

Scene scene_start_create(void) {
    Scene scene;
    memset(&scene, 0, sizeof(Scene));
    scene.name        = "Start";
    scene.initialize  = &init;
    scene.update      = &update;
    scene.draw        = &draw;
    scene.destroy     = &destroy;
    scene.on_key_down = &on_key_down;
    game_log("Start scene created");
    return scene;
}
