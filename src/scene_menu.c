#include "scene_menu.h"
#include "game.h"
#include "scene_settings.h"
#include "scene_start.h"
#include "shared.h"
#include "utility.h"
#include "button.h"
#include <allegro5/allegro.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_image.h>

#include <stdio.h>

static const char* txt_title = "Space Shooter";
static const char* txt_info  = "Press enter key to start";

static void init(void);
static void draw(void);
static void destroy(void);
static void on_key_down(int keycode);

static ALLEGRO_SAMPLE_ID bgm_id;
static Button settings_btn;

static void init(void) {
    settings_btn =
        new_button(SCREEN_W - 48, 10, 38, 38, img_settings, img_settings2);
    bgm_id = play_bgm(bgm, get_volume());
    game_log("Menu scene initialized");
}

static void draw(void) {
    al_draw_bitmap(img_menu_background, 0, 0, 0);

    draw_button(&settings_btn);

    al_draw_text(font_pirulen_32, al_map_rgb(255, 255, 255), SCREEN_W / 2, 30,
                 ALLEGRO_ALIGN_CENTER, txt_title);
    al_draw_text(font_pirulen_24, al_map_rgb(255, 255, 255), 20, SCREEN_H - 50,
                 0, txt_info);
}

static void destroy(void) {
    stop_bgm(bgm_id);
    game_log("Menu scene destroyed");
}

static void on_key_down(int keycode) {
    if (keycode == ALLEGRO_KEY_ENTER)
        game_change_scene(scene_start_create());
}

static void on_mouse_down(int btn, int x, int y, int dz) {
    if (btn == 1) {
        bool in_rect = pnt_in_rect(mouse_x, mouse_y, SCREEN_W - 48, 10, 38, 38);
        if (in_rect)
            game_change_scene(scene_settings_create());
    }
}

// TODO: Add more event callback functions such as update, ...

// Functions without 'static', 'extern' prefixes is just a normal
// function, they can be accessed by other files using 'extern'.
// Define your normal function prototypes below.

// The only function that is shared across files.
Scene scene_menu_create(void) {
    Scene scene;
    memset(&scene, 0, sizeof(Scene));
    scene.name          = "Menu";
    scene.initialize    = &init;
    scene.draw          = &draw;
    scene.destroy       = &destroy;
    scene.on_key_down   = &on_key_down;
    scene.on_mouse_down = &on_mouse_down;
    game_log("Menu scene created");
    return scene;
}
