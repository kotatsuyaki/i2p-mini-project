// [shared.c]
// you should define the shared variable declared in the header here.

#include "shared.h"
#include "game.h"
#include "scene_menu.h"
#include "utility.h"

#include <allegro5/allegro_primitives.h>

ALLEGRO_FONT* font_pirulen_32;
ALLEGRO_FONT* font_pirulen_24;

static void init_game_resource() {
    img_plane      = load_bitmap("assets/plane.png");
    img_sky        = load_bitmap("assets/sky.jpg");
    img_bullet     = load_bitmap("assets/bullet.png");
    img_background = load_bitmap("assets/start-bg.jpg");
    img_enemy      = load_bitmap("assets/smallfighter0006.png");
    game_bgm       = load_audio("assets/mythica.ogg");
    explosion_fx   = load_audio("assets/explosion.wav");
    beam_fx        = load_audio("assets/beam.wav");
}

static void init_gameover_resource() {
    img_main_menu         = load_bitmap("assets/menu-button.png");
    img_main_menu_pressed = load_bitmap("assets/menu-button-pressed.png");
    img_gameover_background =
        load_bitmap_resized("assets/go-bg.jpg", SCREEN_W, SCREEN_H);
    over_bgm = al_load_sample("assets/gameover_bgm.wav");
}

static void init_menu_resource() {
    img_menu_background =
        load_bitmap_resized("assets/main-bg.jpg", SCREEN_W, SCREEN_H);
    img_settings  = load_bitmap("assets/settings.png");
    img_settings2 = load_bitmap("assets/settings2.png");
    bgm           = load_audio("assets/S31-Night Prowler.ogg");

    img_mute = load_bitmap("assets/mute-button.png");
    img_mute_p = load_bitmap("assets/mute-button-pressed.png");
    img_unmute = load_bitmap("assets/unmute-button.png");
    img_unmute_p = load_bitmap("assets/unmute-button-pressed.png");
}

void shared_init(void) {
    font_pirulen_32 = load_font("assets/pirulen.ttf", 32);
    font_pirulen_24 = load_font("assets/pirulen.ttf", 24);
    font_fmr = load_font("assets/fmr.ttf", 18);
    init_menu_resource();
    init_game_resource();
    init_gameover_resource();
    game_change_scene(scene_menu_create());
}

void shared_destroy(void) {
    al_destroy_font(font_pirulen_32);
    al_destroy_font(font_pirulen_24);
    al_destroy_bitmap(img_plane);
    al_destroy_bitmap(img_bullet);
    al_destroy_bitmap(img_background);
    al_destroy_bitmap(img_enemy);
    al_destroy_sample(bgm);
    al_destroy_bitmap(img_menu_background);
    al_destroy_bitmap(img_settings);
    al_destroy_bitmap(img_settings2);
}

bool one_out_of(int denom) {
    if (rand() % denom)
        return false;
    return true;
}

int rand_sign() {
    if (rand() % 2)
        return -1;
    return 1;
}

ALLEGRO_COLOR interpolate_color(ALLEGRO_COLOR ca, ALLEGRO_COLOR cb,
                                ALLEGRO_COLOR cc, float ratio) {
    ALLEGRO_COLOR color;
    if (ratio <= 0.5) {
        ratio *= 2;
        color = al_map_rgba_f(cb.r * ratio + ca.r * (1.0f - ratio),
                              cb.g * ratio + ca.g * (1.0f - ratio),
                              cb.b * ratio + ca.b * (1.0f - ratio),
                              cb.a * ratio + ca.a * (1.0f - ratio));
    } else {
        ratio -= 0.5;
        ratio *= 2;
        color = al_map_rgba_f(cc.r * ratio + cb.r * (1.0f - ratio),
                              cc.g * ratio + cb.g * (1.0f - ratio),
                              cc.b * ratio + cb.b * (1.0f - ratio),
                              cc.a * ratio + cb.a * (1.0f - ratio));
    }
    return color;
}

void draw_grey_bg_box() {
    int rect_xi = 20;
    int rect_yi = 85;
    int rect_xf = SCREEN_W - 20;
    int rect_yf = SCREEN_H - 100;

    al_draw_filled_rectangle(rect_xi, rect_yi, rect_xf, rect_yf,
                             al_map_rgba(80, 80, 80, 150));
}

void draw_title(const char* title) {
    al_draw_text(font_pirulen_32, al_map_rgb(255, 255, 255), SCREEN_W / 2, 30,
                 ALLEGRO_ALIGN_CENTER, title);
    al_draw_filled_rectangle(20, 70, SCREEN_W - 20, 72,
                             al_map_rgb(0xff, 0xff, 0xff));
}

bool muted = false;

float get_volume() {
    if (muted)
        return 0.0f;
    return 1.0f;
}
